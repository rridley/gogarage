module git.sr.ht/~rridley/gogarage

go 1.18

require (
	github.com/warthog618/gpiod v0.8.0 // indirect
	golang.org/x/sys v0.0.0-20200223170610-d5e6a3e2c0ae // indirect
)
