package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/warthog618/gpiod"
)

type Status struct {
	Status bool `json:"status"`
}

func main() {
	relay, _ := gpiod.RequestLine("gpiochip0", 23, gpiod.AsOutput(0))
	reedSwitch, _ := gpiod.RequestLine("gpiochip0", 4)

	http.HandleFunc("/garage/toggle", func(w http.ResponseWriter, r *http.Request) {
		relay.SetValue(1)
		time.Sleep(500 * time.Millisecond)
		relay.SetValue(0)

		fmt.Fprintf(w, "Done")
	})

	http.HandleFunc("/garage/status", func(w http.ResponseWriter, r *http.Request) {
		current, _ := reedSwitch.Value()

		result := Status{
			Status: (current != 1),
		}

		txt, _ := json.Marshal(result)

		w.Write(txt)
	})

	http.ListenAndServe(":3000", nil)
}
